# DaVinci Lessons

This is a first draft of some introductory lessons to the DaVinci software for the LHCb, taught during the [LHCb Starterkit in November 2022](https://indico.cern.ch/event/1206471/timetable/?view=standard). They are largely based on the code and the lessons from the [First Analysis Steps](https://lhcb.github.io/starterkit-lessons/first-analysis-steps/README.html), with some re-ordering and new code in place. 

This is meant to be self-explanatory, but do not hesitate contacting me or dropping a message on the [Mattermost channel](https://mattermost.web.cern.ch/starterkit/channels/starterkit-2022) with any questions. They are meant to be updated and improved with time.

Here is the basic structure of the repository:

- [intro_dv.md](intro_dv.md) is the main lesson, structured in different sections that are meant to be taught sequentially. 
- [code](code) contains the full Python scripts for each of the sections from [intro_dv.md](intro_dv.md).

Author: Miguel Fernández Gómez, based on the LHCb Starterkit lessons.