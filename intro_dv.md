# Introduction to the DaVinci software

As mentioned in previous lessons, the Brunel application is responsible for transforming the triggered, raw data from the detector into large files called DSTs, which contain the full reconstruction information of the events. For our analysis, we will be applying selections to the reconstructed events so that the data files we eventually manage are more manageable from a computing point of view. They are ROOT files, and to go from DSTs to ROOT files, we use the DaVinci software.

DSTs exist for both simulated data and real data collected by the LHCb experiment. If you've been following the lessons up until this point, you should already have at least one DST file downloaded on your local directories from the grid. Now, we want to apply a stripping selection to filter the events inside this DST we want for our analysis.

To produce the output ROOT files, or ntuples, we need to send DaVinci some instructions so that it can operate properly. We do this through the _options file_. Our main object of use will be the class `DecayTreeTuple`, which will create a TTree inside the ROOT file with the information we ask for. Lastly, we need our stripping line, the filter through which we will be selecting the desired candidates from our Monte Carlo DST. In our case, we will be using the stripping line `D2hhPromptDst2D2KKLine`

Let us create a new file named `ntuple_options.py`, and write:

```python
from Configurables import DecayTreeTuple

## Specify the stream and stripping line
stream = "AllStreams"
line   = "D2hhPromptDst2D2KKLine"

## We create the DecayTreeTuple object, and indicate the Input 
## (i.e., the TES location where the desired candidates may be)
## as well as the decay descriptor
dtt = DecayTreeTuple("TupleDstToD0pi_D0ToKK")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
dtt.Decay  = "[D*(2010)+ -> (D0 -> K- K+) pi+]CC"
```

We now use the `DaVinci` class to set the configuration attributes. We add the following lines to `ntuple_options.py`:

```python
from Configurables import DaVinci

DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType       = "DST"
DaVinci().TupleFile       = "DVntuple.root"
DaVinci().PrintFreq       = 1000
DaVinci().DataType        = "2016"
DaVinci().Simulation      = True
DaVinci().Lumi            = not DaVinci().Simulation
DaVinci().EvtMax          = -1
DaVinci().CondDBtag       = "sim-20170721-2-vc-md100"
DaVinci().DDDBtag         = "dddb-20170721-3"
```

Here is a quick breakdown of these attributes:

- `UserAlgorithms` indicates the algorithm to be run over the events.
- `InputType` should be `'DST'` for DST files (and `'MDST'` for microDST).
- `TupleFile` is the name of the output ROOT file, where the TTree will be stored.
- `PrintFreq` is the frequency of events with which DaVinci will print the status.
- `DataType` is the year of data-taking this corresponds to.
- `Simulation` is either `True` when dealing with Monte Carlo files, or `False` when using LHCb-taken data.
- `Lumi` is set to `True` if we want to store information on the integrated luminosity. It is usually set to the opposite value as `Simulation`, as we are only interested in getting the integrated luminosity value for data files. This is because it is a way to check whether or not we have run over the full dataset, as we can compare this value to the integrated luminosity of each year.
- `EvtMax` is the number of events to run over, where `-1` indicates to run over all events.
- `CondDBtag` and `DDDBtag` are the exact detector conditions that the Monte Carlo was generated with. It contains, for instance, information on the magnet polarity. 

---
## Database tags
Generally, the `CondDB` and `DDDB` tags are different for each dataset you
want to use, but will be the same for all DSTs within a given dataset.

For real collision data, you shouldn't specify these tags, as the default
tags are the latest and greatest, so just remove those lines from the options
file.

However, when using simulated data, *always* find out what the database tags are for
your dataset!

There are several ways to access the database tags used for a specific production, but the most reliable one consists of the following steps:
- Find the bookkeeping location of any DST for your desired event type and conditions (e.g. `/lhcb/MC/2016/ALLSTREAMS.DST/00070793/0000/00070793_00000002_7.AllStreams.dst`).
- The number after `ALLSTREAMS.DST` is the number of the production: in this case, `00070793`.
- Go to the [transformation monitor](https://lhcb-portal-dirac.cern.ch/DIRAC/?view=tabs&theme=Grey&url_state=1|*LHCbDIRAC.LHCbTransformationMonitor.classes.LHCbTransformationMonitor:,). Put this number in the field `ProductionID(s):` and press "Submit". You will see the details of the production to the right.
- Right click on these details, and press "Show request". The new tab "Production Request manager" will appear to the right of the "LHCb Transformation Monitor". Go to that tab.
- You will see the details of the MC request. Right click on it, and press "View".
- A new window will pop up with the complete details of the request. You have to find the "Step 1" section, and the following line in it `DDDB: dddb-20170721-3 Condition DB: sim-20170721-2-vc-md100` contains your database tags.

Note that the Condition DB tags for different magnet polarities are different: `-md100` should be replaced by `-mu100` for the MagUp conditions. 

This method can also be used to find other details about how any data was processed by DIRAC, such as the options files and application versions.

---

Finally, we need to tell DaVinci which files we want to run over these algorithms. We already have downloaded a DST, so assuming it's on the same directory as `ntuple_options.py`, we simply add:

```python
from GaudiConf import IOHelper

IOHelper().inputFiles([
	"./00070793_00000001_7.AllStreams.dst"
], clear=True)
```

We are now ready to run DaVinci. Going back to the terminal, to the directory where both the `ntuple_options.py` module and the `.dst` file are stored, we simply type:

```shell
$ lb-run DaVinci/v46r4 gaudirun.py ntuple_options.py
```

The full options file we've created, `ntuple_options.py`, is [available here](./code/minimal-dv/ntuple_options.py). A slightly modified version that uses remote files (using an XML catalog as [described here](files-from-grid)) is [available here](./code/minimal-dv/ntuple_options_xmlcatalog.py).

Once the DaVinci process has ended, we should have a new `DVntuple.root` file in the current directory.

## Getting more detailed information

As the script is right now, the output ROOT file will have some basic information on the mother particle, `D*`, by default. To add information on the rest of the particles on the decay, we add a `^` before each of them. The exception comes when we have a parenthesis, in which case we add it right before the parenthesis to indicate that we want information on the mother particle of the sub-decay. In other words, we simply write:

```python
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
```

If we run the code again, we will see that we have a lot more branches on the output ROOT file. The names of the branches are automatically assigned by DaVinci, but we can rename them by hand. To do that, we first have to import `addBranches` from `DecayTreeTuple.Configuration` and then pass this function a dictionary where the keys are the desired names for the particles, and the values are the decay descriptor where we mark the particle in question. The code is as follows:

```python
from DecayTreeTuple.Configuration import addBranches

dtt.addBranches({"Dstar" : "[D*(2010)+ -> (D0 -> K- K+) pi+]CC",
                 "D0"    : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC",
                 "Kminus": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kplus" : "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pisoft": "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})
```

# TupleTools

We can add as much information to our ntuples as we like. The handling of additional information is done through `TupleTools`, which are C++ classes that help us fill in the output ROOT file. Here is a quick breakdown of some of the basic ones:

 - `TupleToolKinematic`, which fills the kinematic information of the decay;
 - `TupleToolPid`, which stores DLL and PID information of the particle;
 - `TupleToolANNPID`, which stores the new NeuralNet-based PID information of the particle;
 - `TupleToolGeometry`, which stores the geometrical variables (IP, vertex position, etc) of the particle;
 - `TupleToolEventInfo`, which stores general information (event number, run number, GPS time, etc) of the event;
 - `TupleToolTrackInfo`, which fills the tracking information of our particles;
 - `TupleToolPrimaries`, which stores the information of the PV associated to our particle.

We can add a basic set of TupleTools to our `DecayTreeTuple` object as follows:

```python
dtt.ToolList = ["TupleToolEventInfo",
	        "TupleToolANNPID",
		"TupleToolGeometry",
		"TupleToolKinematic"]
```

We can also explore further configuration of certain TupleTools. To do that, we use the method `addTupleTool`, and we have the option of storing the output in a variable, so that we can later set more attributes. For instance, we can add the TupleTool `TupleToolTrackInfo` to fill the tracking information of our particles. But additionally, we also have the option of storing further information from the tracks, such as the number of degrees of freedom of the track fit. To do that, we set the `Verbose` attribute as `True`:

```python
track_tool         = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True
```

It is not necessary to create a new variable, though, and we can simply type the following if we wanted to keep PV information:

```python
dtt.addTupleTool("TupleToolPrimaries")
```

We can also add specific information on simply one of the particles. Consider a situation in which we wanted to store the proper time information of the mother particle, D0. We would simply type:

```python
dtt.D0.addTupleTool("TupleToolPropertime")
```

There are two big asterisks here. First of all, the structure of the variable that calls for `addTupleTool` must be `dtt.PARTICLENAME`, where `PARTICLENAME` is the exact name that is in the `addBranches` dictionary, without the string quotes. Say that the value we passed the dictionary comes from a previously defined variable, `d0name`. We can rewrite the above line as follows:

```python
### ALTERNATIVE OPTION ###
d0name = "D0"
getattr(dtt, d0name).addTupleTool("TupleToolPropertime")
```

Both options are completely equivalent. 

The second asterisk is the fact that this TupleTool may raise an error if the D0 does not come from the PV. We have to ensure that this is the case, and for that we change the `UserAlgorithms` line to this:

```python
from Configurables import CheckPV
DaVinci().UserAlgorithms += [CheckPV(), dtt]
```

There is another, more sophisticated approach to ask for the Primary Vertex. The `CheckPV()` function should work in most cases, but at the end of the day, it's another black box. Alternatively, we can use a `LoKi__VoidVilter` object and ask for the events to contain a specific TES location:

```python
### ALTERNATIVE OPTION ###
from Configurables import LoKi__VoidFilter

pv = LoKi__VoidFilter("hasPV",Code="CONTAINS('Rec/Vertex/Primary')>0")
DaVinci().UserAlgorithms += [pv, dtt]
```

Before we move on, one final note. It is possible that, under this set up, there are events that fail to meet the filter but go on to apply the `DecayTreeTuple` algorithms, and therefore crash when adding `TupleToolPropertime`. To make sure that the algorithms we want to run are processed sequentially, we can use a `GaudiSequencer`. It's quite easy to use:

```python
from Configurables import LoKi__VoidFilter, GaudiSequencer

pv = LoKi__VoidFilter("hasPV",Code="CONTAINS('Rec/Vertex/Primary')>0")
gs = GaudiSequencer("myseq")
gs.Members += [pv, dtt]

DaVinci().UserAlgorithms += [gs]
```

---
## Where to find TupleTools

One of the most difficult things is to know which tool we need to add to our 
`DecayTreeTuple` in order to get the information we want.
For this, it is necessary to know where to find `TupleTools` and their code.
`TupleTools` are spread in 9 packages under `Analysis/Phys` (see the master branch in `git` [here](https://gitlab.cern.ch/lhcb/Analysis/tree/run2-patches/Phys)), all starting with the prefix `DecayTreeTuple`, according to the type of information they fill in our ntuple:

- `DecayTreeTuple` for the more general tools;
- `DecayTreeTupleANNPID` for the NeuralNet-based PID tools;
- `DecayTreeTupleDalitz` for Dalitz analysis;
- `DecayTreeTupleJets` for obtaining information on jets;
- `DecayTreeTupleMC` gives us access to MC-level information;
- `DecayTreeTupleMuonCalib` for muon calibration tools;
- `DecayTreeTupleReco` for reconstruction-level information, such as `TupleToolTrackInfo`;
- `DecayTreeTupleTracking` for more detailed tools regarding tracking;
- `DecayTreeTupleTrigger` for accessing to the trigger information of the candidates.

The `TupleTools` are placed in the `src` folder within each package and it's usually easy to get what they do just by looking at their name.
However, the best way to know what a tool does is check its documentation, either by opening its `.h` file or be searching for it in the latest [`doxygen`](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/index.html).
Most tools are very well documented and will also inform you of their configuration options.
As an example, to get the information on the `TupleToolTrackInfo` we used before we could either check its [source code](https://gitlab.cern.ch/lhcb/Analysis/blob/run2-patches/Phys/DecayTreeTupleReco/src/TupleToolTrackInfo.h) or its [web documentation](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/da/ddd/class_tuple_tool_track_info.html).
 In case we need more information or need to know *exactly* what the code does, the `fill` method is the one we need to look at.

 As a shortcut, the list of tupletools can also be found in doxygen at the top of the pages for the [`IParticleTupleTool`](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/de/df8/struct_i_particle_tuple_tool.html) and the [`IEventTupleTool`](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/d5/d88/struct_i_event_tuple_tool.html) interfaces (depending on whether they fill information about specific particles or the event in general).

---

# LoKi Functors

LoKi functors allow us to obtain even more specific information from the DST. If you've ever seen the details on how a stripping line is configured, you may have already come across these objects. (You can access the configuration to all the stripping lines [on this link](https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/)) They are our language to interact with the DST and apply cuts before we introduce them into our output ROOT file. Roughly speaking, LoKi functors are to DSTs what Branches are to ROOT files. It is important to realize, however, that not all LoKi functors have a direct translation into branches in our ROOT files, which can sometimes be the source of big headaches.

Let's say, for instance, that we are interested in adding a variable to the ROOT file that includes the minimum momentum of the daughters. To do that, we simply have to create our own TupleTool and add a specific variable that we create through the use of LoKi functors. Here is an example where we add both the minimum and maximum momentum of the daughters:

```python
mainTools = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/MyCustomVars")
mainTools.Variables = {
        "MinDght_P": "PFUNA(AMINCHILD(P))",
        "MaxDght_P": "PFUNA(AMAXCHILD(P))",
}
```

There are three LoKi functors at play here. The `P` functor collects the momentum of the particles, the `AMINCHILD()` (or `AMAXCHILD()`) sets the minimum (or maximum) of the given functor for the daughter particles. Finally, `PFUNA` is a bit more technical, but essentially allows for the conversion between the array-like comparison that `AMINCHILD()` performs and the output variable. As previously seen, the keys of the dictionary are the names of the branches on the output ROOT file.

## Much more on LoKi functors to come

Working with LoKi functors is an essential part of dealing with ntuple-making and DaVinci. You can find some documentation about what each of them does [on this link](https://lhcb-doxygen.web.cern.ch/lhcb-doxygen/davinci/latest/d7/dae/namespace_lo_ki_1_1_cuts.html). The lesson on the official Starterkit page, [`Fun with LoKi functors`](https://lhcb.github.io/starterkit-lessons/first-analysis-steps/loki-functors.html), deals with this a bit differently. They use it to interact directly with the DST file and read off values from the actual events. It is a more intuitive approach as it deals directly with the original data, but I prefered not to include it to make the lessons be less confusing and more focused. 

LoKi functors are an essential part of applying stripping cuts at the DST level, and they will come back in slightly more advanced parts of DaVinci.

# Monte Carlo Decay Tree Tuples

As we've seen in the Dataflow for Run 2 lesson, the first step of the Monte Carlo production is the particle simulation done under the Gauss framework. This is the most basic step of the process, as it does not involve the LHCb detector whatsoever. We can actually access the information generated here and create a new TTree object on our output ROOT file. 

Why would we be interested in this? In one word: Efficiencies! Getting a handle of our efficiencies in the analysis is absolutely key, and it might also be useful to expand them and know the efficiency of each step of the process (reconstruction, stripping, trigger, etc.) Let's say we want to compute the stripping efficiency. We need to know how many events have passed the stripping line over how many events we had in total. The <em>total number of events</em> is given by the `MCDecayTreeTuple`. Here is how to implement it:

```python
from Configurables                import MCDecayTreeTuple
from DecayTreeTuple.Configuration import addBranches
    
mctuple        = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay  = "[D*(2010)+ => (D0 ==> K- K+) pi+]CC"
## Add here further configuration (branch names, TupleTools, etc.)

DaVinci().UserAlgorithms += [mctuple]
```

Alternatively, if we are using a GaudiSequencer, this would be the first item on the sequence. Notice also that the decay descriptor is now using the 'newer' LoKi decay descriptor syntax. You can check out more information [here](https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders).

# Using Catalogs

Even though we downloaded our DST files in our previous session to have quicker access to our data and test our DaVinci conde, in general, there is no need to get our hands dirty downloading DST files. This can be especially tedious and space-consuming when working with multiple decays and multiple years. We can always access DST files directly from the grid through DaVinci, by using catalogs.

Let us first create a directory called `catalogs`, and move our `*ALLSTREAMS.DST.py` file that we downloaded from DIRAC in there. We will create a copy that we call `testcatalog.py`, open it and delete all LFNs except for the first two, which we will use for our testing.

After that, we go back to the terminal and run the following command:

```
$ lb-dirac dirac-bookkeeping-genXMLCatalog --Options=testcatalog.py --Catalog=myCatalog.xml
```

We can now go into our options file and substitute the `IOHelper` line(s) with the following two lines:

```python
from Gaudi.Configuration import FileCatalog
FileCatalog().Catalogs = [“xmlcatalog_file:catalogs/mycatalog.xml”]
```

# Running on data

# DecayTreeFitter

# Reconstruction tuples

# Build your own decay

# Running a different stripping line on Monte Carlo
