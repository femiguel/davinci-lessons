# Basic DaVinci codes

This directory contains the full Python codes for each of the steps of the introductory DaVinci lesson. They follow this order:

 - [intro_dv.py](intro_dv.py), which is the most basic DaVinci code you can make;
 - [intro_dv_branches.py](intro_dv_branches.py), which is an expansion of the previous module, adding new branches to the output ROOT file;
 - [intro_dv_tupletools.py](intro_dv_tupletools.py), which adds TupleTools into the fold;
 - [intro_dv_dtf.py](intro_dv_dtf.py), which adds DecayTreeFitter variables;
 - [intro_dv_dtf_loki.py](intro_dv_dtf_loki.py), which is an expansion of the previous module.

To run any of the codes, we first need the DST file from a previous lesson from the Starterkit, which can be downloaded using the following command:

```
$ lb-dirac dirac-dms-get-file LFN:/lhcb/MC/2016/ALLSTREAMS.DST/00070793/0000/00070793_00000001_7.AllStreams.dst
```

Now, to run the module `<intro_dv.py>`, which can stand for any of the aforementioned modules, we simply type:

```
$ lb-run DaVinci/v46r4 gaudirun.py <intro_dv.py>
```