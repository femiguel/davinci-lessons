from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import addBranches

## Specify the stream and stripping line
stream = "AllStreams"
line   = "D2hhPromptDst2D2KKLine"

## We create the DecayTreeTuple object, and indicate the Input 
## (i.e., the TES location where the desired candidates may be)
## as well as the decay descriptor
dtt = DecayTreeTuple("TupleDstToD0pi_D0ToKK")
dtt.Inputs = ["/Event/{0}/Phys/{1}/Particles".format(stream, line)]
dtt.Decay  = "[D*(2010)+ -> ^(D0 -> ^K- ^K+) ^pi+]CC"
dtt.addBranches({"Dstar" : "[D*(2010)+ -> (D0 -> K- K+) pi+]CC",
                 "D0"    : "[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC",
                 "Kminus": "[D*(2010)+ -> (D0 -> ^K- K+) pi+]CC",
                 "Kplus" : "[D*(2010)+ -> (D0 -> K- ^K+) pi+]CC",
                 "pisoft": "[D*(2010)+ -> (D0 -> K- K+) ^pi+]CC"})

## Add TupleTools

dtt.ToolList = ["TupleToolEventInfo",
                "TupleToolANNPID",
                "TupleToolGeometry",
                "TupleToolKinematic"]

track_tool         = dtt.addTupleTool("TupleToolTrackInfo")
track_tool.Verbose = True

dtt.addTupleTool("TupleToolPrimaries")
dtt.D0.addTupleTool("TupleToolPropertime")

## DTF with LoKi functors

from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import LoKi__Hybrid__DictOfFunctors

DictTuple = dtt.Dstar.addTupleTool(LoKi__Hybrid__Dict2Tuple, "DTFTuple")
DictTuple.addTool(DTFDict, "DTF")
DictTuple.Source = "LoKi::Hybrid::DTFDict/DTF"
DictTuple.NumVar = 10
DictTuple.DTF.constrainToOriginVertex = True
DictTuple.DTF.daughtersToConstrain    = ["D0"]

DictTuple.DTF.addTool(LoKi__Hybrid__DictOfFunctors, "dict")
DictTuple.DTF.Source = "LoKi::Hybrid::DictOfFunctors/dict"
DictTuple.DTF.dict.Variables = {
    "DTFDict_Dstar_PT" : "PT",
    "DTFDict_Dstar_M"  : "M",
    "DTFDict_D0_PT": "CHILD(PT,1)",
    "DTFDict_D0_M" : "CHILD(M, '[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC')",
    "DTFDict_D0_PX": "CHILD(PX, '[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC')",
    "DTFDict_D0_PY": "CHILD(PY, '[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC')",
    "DTFDict_D0_PZ": "CHILD(PZ, '[D*(2010)+ -> ^(D0 -> K- K+) pi+]CC')"
    }
    
from Configurables import DaVinci, CheckPV

DaVinci().UserAlgorithms += [CheckPV(), dtt]
DaVinci().InputType       = "DST"
DaVinci().TupleFile       = "DVntuple.root"
DaVinci().PrintFreq       = 1000
DaVinci().DataType        = "2016"
DaVinci().Simulation      = True
DaVinci().Lumi            = not DaVinci().Simulation
DaVinci().EvtMax          = -1
DaVinci().CondDBtag       = "sim-20170721-2-vc-md100"
DaVinci().DDDBtag         = "dddb-20170721-3"

from GaudiConf import IOHelper

IOHelper().inputFiles([
	"./00070793_00000001_7.AllStreams.dst"
], clear=True)
